import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ControlComponent } from './control/control.component'
import { ProgramComponent } from './program/program.component';

const routes: Routes = [
  { path: 'control', component: ControlComponent},
  { path: 'program', component: ProgramComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
import { Injectable } from '@angular/core'
import { DataClient, ServiceClient } from 'blackbox-client'

enum Button {
  up = "up",
  down = "down",
  left = "left",
  right = "right",
  centre = "centre"
}
interface Basys3 {
  switches?: {
    set: number
    clear: number
  }
  buttons?: {
    pressed: string[]
    released: string[]
  }
}

@Injectable({
  providedIn: 'root'
})
export class Basys3Service {
  private _authentication: any

  constructor() { }
  
  set authentication(auth: any) {
    this._authentication = auth
  }

  get authentication() {
    return this._authentication
  }

  private getBasys3Service(url: string) {
    return new ServiceClient(url).init()
    .then((client: ServiceClient) => <Promise<DataClient<Basys3>>>client.navigateByType({
        "uri": "file:///Users/bmillar/dev/basys3pi/datatypes.json",
        "name": "basys3"
      })
    )
    .then((client: DataClient<Basys3>) => {
      client.authenticationToken = this.authentication
      return client
    })
  }

  getSwitches(url: string): Promise<boolean[] | undefined> {
    return this.getBasys3Service(url)
    .then((client: DataClient<Basys3>) => client.get())
    .then(basys3s => {
      let set = (<Basys3[]>basys3s)?.[0]?.switches?.set
      if(set !== undefined) {
        const switches: boolean[] = Array(16)
        for(let i = 0; i < 16; ++i) {
          switches[i] = (set & 1) !== 0
          set = set >> 1
        }
        return switches
      } else {
        return undefined
      }
    })
  }

  setSwitch(url: string, index: number, state: boolean) {
    this.getBasys3Service(url)
    .then((client: DataClient<Basys3>) => {
      const data = {switches: {set: 0, clear: 0}}
      data.switches[state ? 'set' : 'clear'] = 1 << index
      
      return client.patch('basys3', data)
    })
    .catch((err: any) => console.error(err))
  }

  setButton(url: string, button: string, state: boolean) {
    this.getBasys3Service(url)
    .then((client: DataClient<Basys3>) => {
      const data = {buttons: {pressed: <string[]>[], released: <string[]>[]}}
      data.buttons![state ? 'pressed' : 'released'] = [button]
      return client.patch('basys3', data)
    })
    .catch((err: any) => console.error(err))
  }
}

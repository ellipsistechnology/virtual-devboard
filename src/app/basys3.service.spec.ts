import { TestBed } from '@angular/core/testing';

import { Basys3Service } from './basys3.service';

describe('Basys3Service', () => {
  let service: Basys3Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Basys3Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

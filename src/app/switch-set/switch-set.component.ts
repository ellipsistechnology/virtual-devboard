import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'

export interface SwitchSetEvent {
  changedIndex: number
  state: boolean
}

@Component({
  selector: 'app-switch-set',
  templateUrl: './switch-set.component.html',
  styleUrls: ['./switch-set.component.scss']
})
export class SwitchSetComponent implements OnInit {
  @Input()
  labels!: string[]

  _model!: boolean[]
  @Input()
  set model(m: boolean[]) {
    this._model = m
    this.modelChange.emit(m)
    
  }
  get model() {return this._model}
  
  @Output()
  modelChange = new EventEmitter<boolean[]>()

  @Output()
  switchChange = new EventEmitter<SwitchSetEvent>()

  constructor() { }

  ngOnInit(): void {
  }

  trackByIdx(index: number, obj: any): any {
    return index;
  }

  modelChanged(i: number, event:boolean) {
    this.modelChange.emit(this.model)
    this.switchChange.emit({
      changedIndex: i,
      state: this.model[i]
    })
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchSetComponent } from './switch-set.component';

describe('SwitchSetComponent', () => {
  let component: SwitchSetComponent;
  let fixture: ComponentFixture<SwitchSetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwitchSetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

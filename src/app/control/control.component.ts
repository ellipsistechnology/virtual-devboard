import { Component, OnInit } from '@angular/core'
import { Basys3Service } from '../basys3.service'
import { SwitchSetEvent } from '../switch-set/switch-set.component'
import { Router, ActivatedRoute, ParamMap } from '@angular/router'

const SWITCH_COUNT = 16

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit {
  _url!: string
  get url() { return this._url }
  set url(url: string) {
    this._url = url
    this.basys3Service.getSwitches(url)
    .then(switches => {
      if(switches) {
        this.switches = switches
      }
    })
  }

  title = 'virtual-devboard'
  switches: boolean[] = Array(SWITCH_COUNT).fill(false)
  switchLabels = Array(SWITCH_COUNT).fill(0).map((_, i) => 'SW'+i)

  constructor(
    private basys3Service: Basys3Service, 
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.url = params['url']
      this.basys3Service.authentication = params['token']
    })
  }

  switchesChange(event: SwitchSetEvent) {
    this.basys3Service.setSwitch(this.url, event.changedIndex, event.state)
  }

  click(button: string) {
    this.pressed(button)
    this.released(button)
  }

  pressed(button: string) {
    this.basys3Service.setButton(this.url, button, true)
  }

  released(button: string) {
    this.basys3Service.setButton(this.url, button, false)
  }
}

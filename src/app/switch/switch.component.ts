import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent implements OnInit {

  value!: boolean

  @Input()
  get model() {return this.value}

  set model(m: boolean) {
    this.value = m
    this.modelChange.emit(m)
  }

  @Output()
  modelChange = new EventEmitter<boolean>()

  constructor() { }

  ngOnInit(): void {
  }

}
